import * as core from "express-serve-static-core";
import * as mongoose from 'mongoose';
export default class Bootstrap {
    public app: core.Application;
    public config: any;


    constructor(params: any = {}) {
        ({app: this.app, config: this.config = {}} = params);
    }

    run() {
        let {logger = null, routes = null, bodyParser = null, view = null, db = null} = this.config;
        this.setDbConnections(db);
        this.setRouting(routes);
        this.setLogger(logger);
        this.setView(view);
        this.setBodyParser(bodyParser);
    }

    setDbConnections(db: any) {
        if (db) {
            let {mongodb = null, mysql = null} = db;

            if (mongodb) {
                let {uri = null} = mongodb;
                if (uri) {
                    mongoose.connect(uri, (err) => {
                        if (err) {
                            console.log(err.message);
                            console.log(err);
                        } else {
                            console.log('Connected to MongoDb');
                        }
                    });
                }
            }

        }
    }


    setRouting(routes: any) {
        if (routes)
            routes.map(route => this.setRouter(route.path, route.router))
    }

    setRouter(path: string, router: core.Router) {
        this.app.use(path, router);
    }

    setLogger(logger: any) {
        if (logger)
            this.app.use(logger('dev'));
    }

    setBodyParser(bodyParser: any) {
        if (bodyParser) {
            this.app.use(bodyParser.json());
            this.app.use(bodyParser.urlencoded({
                extended: false
            }));
        }
    }

    setView(view) {
        if (view) {
            this.app.set('views', view.path);
            this.app.set('view engine', view.view_engine);
        }
    }
}
