import IAccountModel from './iaccount';
import AccountSchema from './schema';
import RepositoryBase from '../../../../../core/m/repository';

export default class AccountRepository extends RepositoryBase<IAccountModel> {
    constructor() {
        super(AccountSchema);
    }
}

Object.seal(AccountRepository);