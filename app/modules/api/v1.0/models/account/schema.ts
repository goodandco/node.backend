import * as mongoose from 'mongoose';
import IAccountModel from './iaccount';


let schema = new mongoose.Schema({
        name: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true
        },
        id: {
            type: String,
            required: false
        },
        business_manager_id: {
            type: String,
            required: false
        },
        syncTime: {
            type: Date,
            required: false,
        },
        createdAt: {
            type: Date,
            required: false
        },
        modifiedAt: {
            type: Date,
            required: false
        }
    }).pre('save', function (next) {
        if (this._doc) {
            let doc = <IAccountModel>this._doc,
                now = new Date();
            if (!doc.createdAt) {
                doc.createdAt = now;
            }
            doc.modifiedAt = now;
        }
        next();
        return this;
    }),
    AccountSchema = mongoose.model<IAccountModel>('account', schema, 'accounts', true);
export default AccountSchema;