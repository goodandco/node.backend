import * as mongoose from 'mongoose';

export default interface IAccountModel extends mongoose.Document {
    name: string;
    email: string;
    id: string;
    business_manager_id: string;
    syncTime: Date,
    createdAt: Date;
    modifiedAt: Date;
}