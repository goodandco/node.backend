
import IAccountModel from './account/iaccount';
import AccountRepository from './account/raccount';


export default class AccountModel {

    private _accountModel: IAccountModel;

    constructor(accountModel: IAccountModel) {
        this._accountModel = accountModel;
    }

    get name(): string {
        return this._accountModel.name;
    }

    get id(): string {
        return this._accountModel.id;
    }

    get syncTime(): Date {
        return this._accountModel.syncTime;
    }

    static createModel(params: any): Promise<IAccountModel> {
        return new Promise((resolve, reject) => {

            let {id = null, name = null, business_manager_id = null, email = null} = params;

            if (!id || !name) {
                throw new Error('ID and Name are required');
            }

            let repo = new AccountRepository(),
                account = <IAccountModel>{
                    id: id,
                    name: name,
                    business_manager_id: business_manager_id,
                    email: email
                };

            repo.create(account, (err, res) => {
                if (err)
                    reject(err);
                else
                    resolve(res);
            });

        });
    }

    static findAccount(params: any): Promise<IAccountModel> {
        return new Promise((resolve, reject) => {
            let repo = new AccountRepository();

            repo.find(params).sort({createdAt: -1}).limit(1).exec((err, res) => {
                if (err) {
                    reject(err);
                } else {
                    if (res.length) {
                        resolve(res[0]);
                    } else {
                        resolve(null);
                    }
                }
            });
        });
    }

}

Object.seal(AccountModel);


// AccountModel.createHero('Steve', 'Flying').then((res) => {
//     console.log('### Created Hero ###');
//     console.log(res);
//
//     AccountModel.findHero('Steve').then((res) => {
//         console.log('### Found Hero ###');
//         console.log(res);
//
//         // now update the Hero
//         let hero = <IHeroModel>res;
//         hero.power = 'Invisibility';
//         hero.save((err, res) => {
//             if (err) {
//                 console.log(err.message);
//                 console.log(err);
//             }
//             else {
//                 console.log(res);
//             }
//         });
//     }, (err) => {
//         if (err) {
//             console.log(err.message);
//         }
//     });
// }, (err) => {
//     if (err) {
//         console.log(err.message);
//         console.log(err);
//     }
// });