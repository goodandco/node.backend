import * as express from 'express';
import * as mongodb from 'mongodb';
import * as database from '../../../../../database.json';

let index = express.Router(),
    MongoClient = mongodb.MongoClient,
    dbConfig = (<any>database).dev;


/* GET users listing. */
index.get('/', function (req, res, next) {
    let result = {
        error: null,
        data: null
    };

    new Promise((resolve, reject) => {
        MongoClient.connect(`${dbConfig.driver}://${dbConfig.host}/${dbConfig.database}`, (err, db) => {
            if (err) {
                reject(err);
            }
            let result = [];
            db.collection('messages').find().toArray(function (err, items) {
                result = items.map((item, index) => {
                    return Object.assign({}, item, {number: index});
                });
                db.close();

                resolve(result);
            });
        });
    })
        .then((messages: Array<any>) => {
            result.data = {
                info: {
                    title: 'Messages',
                    count: messages.length
                },
                messages: messages
            };

            res.send(result);
        })
        .catch(e => {
            result.error = e.message;
            res.send(result);
        });


});

export default index;
