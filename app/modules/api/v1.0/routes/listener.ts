import * as express from 'express';
import * as mongodb from 'mongodb';
import * as database from '../../../../../database.json';
import {Queue} from '../services/queue';
import AccountModel from '../models/account';

let listener = express.Router(),
    MongoClient = mongodb.MongoClient,
    dbConfig = (<any>database).dev,
    ObjectId = mongodb.ObjectID,
    insertDocument = function (message, db, callback) {
        db.collection('messages').insertOne(message, function (err, result) {
            console.log("Inserted a document into the messages collection.");
            callback();
        });
    },
    queue = Queue,
    createAccount = () => {
        AccountModel.createModel({
            id: '123',
            name: 'Alex',
            business_manager_id: '234',
            email: 'oleksandr.hudenko@gmail.com'
        })
            .then((res) => {
                console.log('### Created Account ###');
                console.log(res);
            })
            .catch((e) => {
                console.log('### Failed Created Account ###');
                console.log(e.message);
            });;
    };

/* GET users listing. */
listener.get('/', function (req, res, next) {
    let result = {
            error: null,
            data: 'ok'
        },
        q = 'start';
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);

    queue.listen(q, (msg) => {
        let msgContent = msg.content.toString();
        console.log(" [x] Received %s", msgContent);
        if (msgContent == 'account') {
            createAccount();
        } else {
            MongoClient.connect(`${dbConfig.driver}://${dbConfig.host}/${dbConfig.database}`, function (err, db) {
                if (err !== null) {
                    throw new Error(err);
                }
                let model = {message: msgContent};
                insertDocument(model, db, function () {
                    db.close();
                });
            });
        }
    });

    res.send(result);

});


export default listener;
