import * as express from 'express';
// import * as amqp from 'amqplib/callback_api';
var amqp = require('amqplib/callback_api');
let get = express.Router();


/* GET users listing. */
get.get('/', function (req, res, next) {
    let result = {
        error: null,
        data: null
    };

    new Promise((resolve, reject) => {
        amqp.connect('amqp://localhost', function (err, conn) {
            conn.createChannel(function (err, ch) {
                let q = 'start',
                    processes = [
                        'account',
                        'daily_statistics',
                        'month_statistics',
                        '28_days_statistics',
                        'campaigns',
                        'adsets',
                        'ads',
                        'creatives',
                        'adimages',
                        'advideos',
                        'appsflier_statistics'
                    ];

                ch.assertQueue(q, {durable: false});
                processes.map(process => ch.sendToQueue(q, new Buffer(process)))
                console.log(" [x] Application has been started");
            });

            setTimeout(function () {
                conn.close();
            }, 500);
            resolve('ok');
        });
    })
        .then(data => {
            result.data = data;
            res.send(result);
        })
        .catch(e => {
            result.error = e.message;
            res.send(result);
        });
});

export default get;