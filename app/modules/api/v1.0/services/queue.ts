import * as amqp from 'amqplib/callback_api';

export default class QueueObject {
    protocol: string = 'amqp';
    host: string = 'localhost';

    listen(queue: string, worker: Function) {
        amqp.connect(`${this.protocol}://${this.host}`, function (err, conn) {
            if (err)
                throw new Error(err);

            conn.createChannel(function (err, ch) {
                if (err)
                    throw new Error(err);

                ch.consume(queue, worker, {noAck: true});
            });
        });
    }


}


export var Queue = new QueueObject();