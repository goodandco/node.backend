import * as routes from '../app/modules/api/v1.0/routes/routes';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';

const root = require('app-root-path').path;

const config = {
    development: {
        db: {
            mongodb: {
                uri: 'mongodb://localhost/social_ads_manager'
            }
        },
        queue: {
            protocol: 'amqp',
            host: 'localhost',
            port: ''
        },
        routes: [
            {path: '/', router: routes.index},
            {path: '/get', router: routes.get},
            {path: '/listen', router: routes.listener}
        ],
        logger: logger,
        bodyParser: bodyParser,
        view: {
            path: `${root}\\views`,
            view_engine: 'twig'
        }
    }
};
export default config;
